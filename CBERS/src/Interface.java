import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.JButton;


public class Interface extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID1 = 1L;
	private JPanel contentPane;

	public static void main1(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

/* RELOGIO
	private static final long serialVersionUID = 1L;

	// Local onde atualizaremos a hora  
    private JLabel lblHora;  
  
    // Formatador da hora  
    private static final DateFormat FORMATO = new SimpleDateFormat("HH:mm:ss");  
  
    public void MyFrame() {  
        // Constru�mos nosso frame  
        
        setLayout(new FlowLayout());  
        getContentPane().add(getLblHora());  
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        setSize(200, 75);  
        setVisible(true);  
  
        // Iniciamos a thread do rel�gio. Tornei uma deamon thread para que seja  
        // automaticamente finalizada caso a aplica��o feche.  
        Thread clockThread = new Thread(new ClockRunnable(), "Clock thread");  
        clockThread.setDaemon(true);  
        clockThread.start();  
    }  
  
    private JLabel getLblHora() {  
        if (lblHora == null) {  
            lblHora = new JLabel();  
        }  
        return lblHora;  
    }  
  

    public void setHora(Date date) {  
        getLblHora().setText(FORMATO.format(date));  
    }  

    private class ClockRunnable implements Runnable {  
        public void run() {  
            try {  
                while (true) {  
                    // Aqui chamamos o setHora atrav�s da EventQueue da AWT.  
                    // Conforme dito, isso garante Thread safety para o Swing.  
                    EventQueue.invokeLater(new Runnable() {  
                        public void run() {  
                            // S� podemos chamar setHora diretamente dessa  
                            // forma, pois esse Runnable � uma InnerClass n�o  
                            // est�tica.  
                            setHora(new Date());  
                        }  
                    });  
                    // Fazemos nossa thread dormir por um segundo, liberando o  
                    // processador para outras threads processarem.  
                    Thread.sleep(1000);  
                }  
            }  
            catch (InterruptedException e) {  
            }  
        }  
    }  
  
    public static void main(String args[]) {  
        new MyFrame();  
    }  
}  */
	
	public Interface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 772, 506);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(5, 5, 746, 313);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Dados Brutos", null, panel, null);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Exportar Dados", null, panel_3, null);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Hist�rico", null, panel_1, null);
		
		JTextPane txtpnHistrico = new JTextPane();
		txtpnHistrico.setText("Hist\u00F3rico");
		panel_1.add(txtpnHistrico);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Menu Inicial", null, panel_2, null);
		
		JLabel lblNewLabel = new JLabel("Esta��o Solo CBERS 4");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 20));
		lblNewLabel.setBounds(70,36,372,37);
		panel_2.add(lblNewLabel);
		
		JLabel lblEsteUm = new JLabel("\r\n\r\n\r\n\r\n\r\n\r\n\r\nEste \u00E9 um programa de atualiza\u00E7\u00E3o de dados e recebimento de sinais do sat\u00E9lite CBERS 4.");
		lblEsteUm.setBounds(43, 111, 470, 15);
		lblEsteUm.setFont(new Font("Dialog", Font.PLAIN, 14));
		panel_2.add(lblEsteUm);
		
		JLabel lblElaboradoPorDaniel = new JLabel("Elaborado por Daniel Moreira e Let\u00EDcia Munhoz");
		panel_2.add(lblElaboradoPorDaniel);
		
		JLabel lblContatoDanielssmbrgmailcom = new JLabel("(danielssmbr@gmail.com leticialmunhoz@gmail.com)");
		panel_2.add(lblContatoDanielssmbrgmailcom);
	}
	
	
	
}
