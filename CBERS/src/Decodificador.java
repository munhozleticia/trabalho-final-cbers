import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class Decodificador {
	private Dados umDado;
	
	public String lerArquivo() throws IOException{
		InputStream fileIn = umDado.getArquivo();
		fileIn = new FileInputStream("telemetria.txt");
		InputStreamReader fileMed = new InputStreamReader(fileIn);
		BufferedReader fileFinal = new BufferedReader(fileMed);
		String texto = fileFinal.readLine();
		
		while(fileFinal.readLine() != null) {
			texto = texto + fileFinal.readLine();
		}
		fileFinal.close();
		return texto;
	}
	
	public void decodificar(String texto) {
		int len = texto.length();
		char[] charArray = new char[len];
		int[] num = new int[7];
		String[] textArray = new String[7];
		// Separar cada array texto para cada dado
		for (int i = 0; i<7; i++) {
			for (int j = 23*i; j < 23*(i+1); j++) {
				textArray[i] = (textArray[i] + charArray[j]);
	        }
		}
		// Convertendo dados para inteiro
		for (int i = 0; i<7; i++) {
			num[i] = Integer.parseInt(textArray[i]);
		}
		// Transformando de binário para decimal
		for (int i = 0; i<7; i++) {
			for (int j = 23*i; j<23*(1+i); j++) {
				num[i] = 2^(-j+23*(i+1))*num[i];
			}
		}
		// Convertendo novamente para String
		for (int i = 0; i<7; i++) {
			textArray[i] = Integer.toString(num[i]);
		}
		// Preenchendo atributos de objeto Dados
		umDado.setAmperagem(textArray[0]);
		umDado.setVoltagem(textArray[1]);
		umDado.setMagnetometro(textArray[2]);
		umDado.setRadiacao(textArray[3]);
		umDado.setNumeroVoltas(textArray[4]);
		umDado.setTemperatura(textArray[5]);
		umDado.setAltura(textArray[6]);
	}
}
