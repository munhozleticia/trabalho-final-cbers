import java.io.InputStream;

public class Dados {

	
	private String amperagem;
	private String voltagem;
	private String magnetometro;
	private String radiacao;
	private String numeroVoltas;
	private String temperatura;
	private String altura;
	private InputStream arquivo;
	private String dadoBruto;
	
	
	
	
	public String getAmperagem() {
		return amperagem;
	}

	public void setAmperagem(String amperagem) {
		this.amperagem = amperagem;
	}

	public String getVoltagem() {
		return voltagem;
	}

	public void setVoltagem(String voltagem) {
		this.voltagem = voltagem;
	}

	public String getMagnetometro() {
		return magnetometro;
	}

	public void setMagnetometro(String magnetometro) {
		this.magnetometro = magnetometro;
	}

	public String getRadiacao() {
		return radiacao;
	}

	public void setRadiacao(String radiacao) {
		this.radiacao = radiacao;
	}

	public String getNumeroVoltas() {
		return numeroVoltas;
	}

	public void setNumeroVoltas(String numeroVoltas) {
		this.numeroVoltas = numeroVoltas;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public InputStream getArquivo() {
		return arquivo;
	}

	public void setArquivo(InputStream arquivo) {
		this.arquivo = arquivo;
	}

	public String getDadoBruto() {
		return dadoBruto;
	}

	public void setDadoBruto(String dadoBruto) {
		this.dadoBruto = dadoBruto;
	}
	
}
