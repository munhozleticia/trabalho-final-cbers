import java.util.ArrayList;

public class Historico extends Dados {


	private String data;
	private String hora;
	private ArrayList<String> listaUltimosDados;
	private ArrayList<Dados> listaDados;
	
	public Historico() {
		
	
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}
	
	public void adicionar(Dados umDado) {
		listaDados.add(umDado);
	}
	
	public String listarHistoricoD() {
		String dadosRegistrados = null;
		for(Dados umDado : listaDados) {
			dadosRegistrados = (this.hora + "  " + this.data + "   " + umDado.getDadoBruto() + "\n" + dadosRegistrados);
		}
		return dadosRegistrados;
	} 
	
	public String ultimaTelemetria() {
		String dadosUltimaTelemetria = null;
		for(Dados umDado : listaDados) {
			dadosUltimaTelemetria = (this.hora + "  " + this.data + "   " + umDado.getDadoBruto() + "\n" + dadosUltimaTelemetria);
			listaUltimosDados.add(umDado.getDadoBruto());
		}
		
		return dadosUltimaTelemetria;
	}
	
	public String listarDadosBrutos() {
		String dadosRegistrados = null;
		for(String umDadoBruto : listaUltimosDados) {
			dadosRegistrados = (umDadoBruto + "\n\n" + dadosRegistrados);
		}
		return dadosRegistrados;
	}

}

