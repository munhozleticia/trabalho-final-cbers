
public class Estados {
	
	private boolean iluminado;
	private boolean torre;
	
	public boolean isIluminado() {
		return iluminado;
	}
	public void setIluminado(boolean iluminado) {
		this.iluminado = iluminado;
	}
	public boolean isTorre() {
		return torre;
	}
	public void setTorre(boolean torre) {
		this.torre = torre;
	}
	public Estados(boolean iluminado, boolean torre) {
		this.iluminado = iluminado;
		this.torre = torre;
	}
	
	/*public static ImageandoTransmitindo(){
		Estado.setIluminado(true);
		
	}*/
	
	
	
}
